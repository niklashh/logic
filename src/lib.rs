use bitvec::prelude::*;
use std::{collections::HashSet, fmt};

const NEGATION: char = '¬';

#[derive(Default, Debug, Clone)]
pub struct Clause(BitVec, Vec<usize>);

impl fmt::Display for Clause {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "(")?;
        let mut first = true;
        for (b, lit) in self.0.iter().zip(self.1.iter()) {
            if !first {
                write!(f, ",")?;
            }
            first = false;
            if !b {
                write!(f, "{}", NEGATION)?;
            }
            write!(f, "{}", lit)?;
        }
        write!(f, ")")?;
        Ok(())
    }
}

impl Clause {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set(&mut self, n: usize, positive: bool) {
        if let Some((ind, _)) = self.1.iter().enumerate().find(|(_, &m)| m == n) {
            self.0.set(ind, positive);
        } else {
            self.0.push(positive);
            self.1.push(n);
        }
    }

    pub fn get(&self, n: &usize) -> Option<bool> {
        self.1
            .iter()
            .zip(self.0.iter())
            .find(|(m, _)| *m == n)
            .map(|(_, pos)| *pos)
    }

    pub fn falsified(&self, a: &Assignment) -> bool {
        for (n, pos) in self.1.iter().zip(self.0.iter()) {
            // Does not take into account multiple instances of a variable
            if let Some(n_val) = a.get(n) {
                if pos == n_val {
                    // clause is satisfied by a
                    return false;
                }
            } else {
                // a does not assign a value to n
                return false;
            }
        }
        // Clause was not satisfied by a
        true
    }

    pub fn satisfied(&self, a: &Assignment) -> bool {
        for (n, pos) in self.1.iter().zip(self.0.iter()) {
            // Does not take into account multiple instances of a variable
            if let Some(n_val) = a.get(n) {
                if pos == n_val {
                    // clause is satisfied by a
                    return true;
                }
            }
        }
        // Clause was not satisfied by a
        false
    }

    pub fn vars(&self) -> HashSet<usize> {
        self.1.iter().copied().collect()
    }
}

#[derive(Default, Debug, Clone)]
pub struct CNF(Vec<Clause>);

impl CNF {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn push(&mut self, c: Clause) {
        self.0.push(c)
    }

    pub fn satisfied(&self, a: &Assignment) -> bool {
        self.0.iter().all(|c| c.satisfied(a))
    }

    pub fn vars(&self) -> HashSet<usize> {
        let mut h = HashSet::new();
        for c in self.0.iter() {
            for v in c.vars() {
                h.insert(v);
            }
        }
        h
    }

    pub fn undef_var(&self, a: &Assignment) -> Option<usize> {
        let self_vars = self.vars();
        let a_vars = a.vars();
        let mut diff = self_vars.difference(&a_vars);
        diff.next().copied()
    }

    pub fn iter(&self) -> std::slice::Iter<Clause> {
        self.0.iter()
    }
}

// TODO derived partialEq doesn't work with perms
#[derive(Default, Debug, Clone, PartialEq)]
/// Seems identical to Clause?
pub struct Assignment(BitVec, Vec<usize>);

impl Assignment {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn is_empty(&self) -> bool {
        self.1.is_empty()
    }

    pub fn set(&mut self, n: usize, positive: bool) {
        if let Some((ind, _)) = self.1.iter().enumerate().find(|(_, &m)| m == n) {
            self.0.set(ind, positive);
        } else {
            self.0.push(positive);
            self.1.push(n);
        }
    }

    pub fn get(&self, n: &usize) -> Option<bool> {
        self.1
            .iter()
            .zip(self.0.iter())
            .find(|(m, _)| *m == n)
            .map(|(_, pos)| *pos)
    }

    pub fn contains(&self, n: &usize) -> bool {
        self.1.contains(n)
    }

    pub fn vars(&self) -> HashSet<usize> {
        self.1.iter().copied().collect()
    }
}

pub fn dpll(f: CNF) -> Option<Assignment> {
    fn _dpll(f: CNF, tau: Assignment) -> Option<Assignment> {
        dbg!(&tau);
        if f.iter().any(|c| c.falsified(&tau)) {
            // Dead-end node, contains a clause falsified by tau
            return None;
        }
        let f_vars = f.vars();
        if f.is_empty() || f_vars.iter().all(|var| tau.contains(var)) {
            // If f contains no clauses or all variables in f are in tau, we have found a solution
            return Some(tau);
        }
        // Select a variable in f not in tau
        let l = f.undef_var(&tau).unwrap();

        let mut tau = tau;
        tau.set(l, true);
        if let Some(tau) = _dpll(f.clone(), tau.clone()) {
            Some(tau)
        } else {
            tau.set(l, false);
            _dpll(f, tau)
        }
    }
    _dpll(f, Assignment::default())
}

#[cfg(test)]
mod test {
    mod clause {
        use crate::*;

        #[test]
        fn display_empty() {
            let c = Clause::default();
            assert_eq!(&format!("{}", c), "()");
        }

        #[test]
        fn display_10_clause() {
            let mut c = Clause::new();
            for n in (0..10).step_by(2) {
                c.set(n, false);
                c.set(n + 1, true);
            }
            assert_eq!(&format!("{}", c), "(¬0,1,¬2,3,¬4,5,¬6,7,¬8,9)");
        }

        #[test]
        fn set() {
            let mut c = Clause::new();
            c.set(2, true);
            c.set(0, false);

            assert_eq!(c.vars().len(), 2);
        }

        #[test]
        fn display_sparse_clause() {
            let mut c = Clause::new();
            c.set(5613, true);
            c.set(17195183, false);
            c.set(usize::MAX, true);
            assert_eq!(&format!("{}", c), "(5613,¬17195183,18446744073709551615)");
        }

        #[test]
        fn empty_clause_always_falsified() {
            let c = Clause::new();
            let a = Assignment::new();
            assert!(c.falsified(&a));

            let mut a = Assignment::new();
            a.set(0, true);
            a.set(1, false);
            assert!(c.falsified(&a));
        }

        #[test]
        fn clause_with_one_var() {
            let mut c = Clause::new();
            c.set(0, true);

            let mut a = Assignment::new();
            assert!(!c.falsified(&a));

            a.set(1, false);
            assert!(!c.falsified(&a));

            a.set(0, true);
            assert!(!c.falsified(&a));

            a.set(0, false);
            assert!(c.falsified(&a));
        }

        #[test]
        fn clause_with_two_vars() {
            let mut c = Clause::new();
            c.set(0, false);
            c.set(1, true);

            let mut a = Assignment::new();
            assert!(!c.falsified(&a));

            a.set(1, false);
            assert!(!c.falsified(&a));

            a.set(0, false);
            assert!(!c.falsified(&a));

            a.set(0, true);
            assert!(c.falsified(&a));
        }

        #[test]
        fn satisfied() {
            let mut c = Clause::new();
            c.set(0, true);

            let mut a = Assignment::new();
            assert!(!c.satisfied(&a));

            a.set(1, true);
            assert!(!c.satisfied(&a));

            a.set(0, false);
            assert!(!c.satisfied(&a));

            a.set(0, true);
            assert!(c.satisfied(&a));
        }
    }

    mod cnf {
        use crate::*;
        #[test]
        fn empty_cnf() {
            let f = CNF::new();
            assert!(f.is_empty());

            assert!(f.vars().is_empty());

            let a = dpll(f.clone()).expect("DPLL to provide an assigment");
            assert!(a.is_empty());
            assert!(f.satisfied(&a));
        }

        #[test]
        fn cnf_with_empty_clause() {
            let mut f = CNF::new();
            f.push(Clause::new());

            assert!(!f.is_empty());

            assert!(f.vars().is_empty());

            let a = dpll(f.clone());
            assert!(a.is_none());
        }

        #[test]
        fn cnf_with_one_clause() {
            let mut f = CNF::new();
            let mut c = Clause::new();
            c.set(0, true);
            c.set(1, false);
            f.push(c);

            assert!(!f.is_empty());

            let mut expected = HashSet::new();
            expected.insert(0);
            expected.insert(1);
            assert_eq!(f.vars(), expected);

            let a = dpll(f.clone()).expect("DPLL to find an assignment");
            assert!(f.satisfied(&a));
        }

        #[test]
        fn cnf_multiple_unsat() {
            let mut f = CNF::new();
            let mut c = Clause::new();
            c.set(0, true);
            f.push(c);
            let mut c = Clause::new();
            c.set(0, false);
            c.set(1, true);
            f.push(c);
            let mut c = Clause::new();
            c.set(1, false);
            c.set(2, true);
            f.push(c);
            let mut c = Clause::new();
            c.set(2, false);
            c.set(0, false);
            f.push(c);

            let mut expected = HashSet::new();
            expected.insert(0);
            expected.insert(1);
            expected.insert(2);
            assert_eq!(f.vars(), expected);

            let a = dpll(f.clone());
            assert_eq!(a, None);
        }

        #[test]
        fn cnf_multiple_sat() {
            let mut f = CNF::new();
            let mut c1 = Clause::new();
            c1.set(0, true);
            f.push(c1.clone());
            let mut c2 = Clause::new();
            c2.set(0, false);
            c2.set(1, true);
            f.push(c2.clone());
            let mut c3 = Clause::new();
            c3.set(1, false);
            c3.set(2, true);
            f.push(c3.clone());
            let mut c4 = Clause::new();
            c4.set(2, false);
            c4.set(0, false);
            c4.set(1, true);
            f.push(c4.clone());

            let mut expected = HashSet::new();
            expected.insert(0);
            expected.insert(1);
            expected.insert(2);
            assert_eq!(f.vars(), expected);

            let a = dpll(f.clone()).expect("DPLL to find an assignment");
            assert!(c1.satisfied(&a));
            assert!(c2.satisfied(&a));
            assert!(c3.satisfied(&a));
            assert!(c4.satisfied(&a));

            assert!(f.satisfied(&a));
        }
    }
}
